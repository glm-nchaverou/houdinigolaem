cmake_minimum_required(VERSION 3.8)
project(HoudiniGolaem CXX)

set(CMAKE_C_COMPILER gcc-6.3)
set(CMAKE_CXX_COMPILER g++-6.3)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
include(Golaem)

list(APPEND CMAKE_PREFIX_PATH "$ENV{HFS}/toolkit/cmake")
find_package(Houdini REQUIRED)

add_definitions(-Wpedantic -Wextra -fPIC -D_GLIBCXX_USE_CXX11_ABI=0)

# Build GolaemCache
add_library(GolaemCache STATIC)
target_sources(GolaemCache
        PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Handles.h
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/GolaemCache.h
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/CacheParameters.h
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Geometry.h
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Simulation.h
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Entity.h
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Entity.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Geometry.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/Simulation.cpp
        ${CMAKE_CURRENT_LIST_DIR}/src/GolaemCache/GolaemCache.cpp
    )

target_include_directories(GolaemCache
        PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}/src
        )

target_link_libraries(GolaemCache
        PUBLIC
        Golaem
        )

# Build HoudiniGolaem
add_library(HoudiniGolaem SHARED)
target_sources(HoudiniGolaem
    PRIVATE
        src/GolaemHoudini/SOP_GolaemImporter.cpp
        src/GolaemHoudini/SOP_GolaemImporter.h
        src/GolaemHoudini/HoudiniGolaem.cpp
        src/GolaemHoudini/GU_PackedGolaem.cpp
        src/GolaemHoudini/GU_PackedGolaem.h
        src/GolaemHoudini/GT_GEOPackedGolaemCollect.cpp
        src/GolaemHoudini/GT_GEOPackedGolaemCollect.h
    )

target_link_libraries(HoudiniGolaem
    PUBLIC
        Houdini
        GolaemCache
    )

#find_package(GTest REQUIRED)
add_subdirectory(test)