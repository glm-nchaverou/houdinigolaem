# Houdini Golaem

This is a sneak preview of HoudiniGolaem! 

It isn't production ready. There are features that are not implemented yet, but I am slowly getting there. 

This project is completly made in spare time, therefore it does not have any time constraints. If you want to support or contact me, drop me an email at piotrbarejko at proton mail.

The **IDEA** is to display and convert Golaem Skeletons or Skins into Houdini geometry. 
The **MOTIVATION** is to use GT interfaces to visualize generated skins and avoid memory copying.  

Here is an example display of PackedGolaems in Houdini:

![PackedGolaems](images/geometryGenerationViewport.gif)
