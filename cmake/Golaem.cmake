if(NOT GOLAEM_ROOT_DIR)
    message(FATAL_ERROR "GOLAEM_ROOT_DIR is not set!")
endif()

message(STATUS "Golaem ROOT - ${GOLAEM_ROOT_DIR}")
message(STATUS "Golaem Include - ${GOLAEM_ROOT_DIR}/devkit/include")
message(STATUS "Golaem Library - ${GOLAEM_ROOT_DIR}/devkit/lib")

# you might want to add extra names
find_library(GolaemCoreLibrary
        NAMES glmCore_gcc482x64
        PATHS ${GOLAEM_ROOT_DIR}/devkit/lib/
        NO_DEFAULT_PATH)

find_library(GolaemCrowdIOLibrary
        NAMES glmCrowdIO
        PATHS ${GOLAEM_ROOT_DIR}/devkit/lib/
        NO_DEFAULT_PATH)

if(NOT GolaemCoreLibrary)
    message(FATAL_ERROR "Golaem Core library not found!")
endif()

if(NOT GolaemCrowdIOLibrary)
    message(FATAL_ERROR "Golaem CrowdIO library not found!")
endif()

message(STATUS "Golaem Core - ${GolaemCoreLibrary}")
message(STATUS "Golaem CrowdIO - ${GolaemCrowdIOLibrary}")

# Create Golaem interface
add_library(Golaem INTERFACE)
target_include_directories(Golaem SYSTEM
        INTERFACE
        ${GOLAEM_ROOT_DIR}/devkit/include
        )

# Core
add_library(Golaem::Core IMPORTED SHARED)
set_target_properties(Golaem::Core
        PROPERTIES
        IMPORTED_LOCATION ${GolaemCoreLibrary}
        )
target_link_libraries(Golaem
        INTERFACE Golaem::Core
        )

# CrowdIO
add_library(Golaem::CrowdIO IMPORTED SHARED)
set_property(TARGET Golaem::CrowdIO
        PROPERTY IMPORTED_LOCATION ${GolaemCrowdIOLibrary}
        )
target_link_libraries(Golaem
        INTERFACE Golaem::CrowdIO z
        )