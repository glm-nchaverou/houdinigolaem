//
// Created by bareya on 02/12/18.
//

#include <climits>
#include <functional>
#include <iostream>
#include <memory>

#define GLMC_IMPLEMENTATION
#include <glm_crowd.h>
#include <glm_crowd_io.h>

#include "GolaemTestPath.h"

void createGeometryGenerationContext(long frameNumber)
{
    int inputFrame = frameNumber;

    {
        GlmGeometryGenerationContext* _geometryContext;

        _geometryContext = static_cast<GlmGeometryGenerationContext*>(GLMC_MALLOC(
            sizeof(GlmGeometryGenerationContext)));
        memset(_geometryContext, 0, sizeof(GlmGeometryGenerationContext));

        _geometryContext->_cacheDirectoryCount = 1;
        _geometryContext->_cacheDirectories = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(
            _geometryContext->_cacheDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
        strcpy(_geometryContext->_cacheDirectories[0], GOLAEM_TEST_CACHE_DIR);

        strcpy(_geometryContext->_cacheName, GOLAEM_TEST_CACHE_NAME);

        _geometryContext->_crowdFieldCount = 1;
        _geometryContext->_crowdFieldNames = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(
            _geometryContext->_crowdFieldCount * GIO_NAME_LENGTH * sizeof(char));
        strcpy(_geometryContext->_crowdFieldNames[0], GOLAEM_TEST_FIELD_NAME);

        _geometryContext->_characterFilesDirectoryCount = 1;
        _geometryContext->_characterFilesDirectories = (char(*)[GIO_NAME_LENGTH])GLMC_MALLOC(
            _geometryContext->_characterFilesDirectoryCount * GIO_NAME_LENGTH * sizeof(char));
        strcpy(_geometryContext->_characterFilesDirectories[0], GOLAEM_TEST_CHAR_GCHA);

        _geometryContext->_frame = inputFrame;
        _geometryContext->_excludedEntityCount = 0;
        _geometryContext->_renderPercent = 1.f;
        strcpy(_geometryContext->_terrainFile, "");
        strcpy(_geometryContext->_layoutFile, GOLAEM_TEST_LAYOUT_PATH);
        _geometryContext->_enableLayout = 1;
        _geometryContext->_maxVerticesPerFace = UINT_MAX; // no max limit to face edge count, can use 4 to limit to quads, or 3 to limit to triangles.
        _geometryContext->_computeEdgeVisibility = 0;     // or 1 to fill _triangleEdgeVisibility, see glm_crowd_io.h

        using GeoGenCtxDeleter = std::function<void(GlmGeometryGenerationContext*)>;
        using GeoGenCtxPtr = std::unique_ptr<GlmGeometryGenerationContext, GeoGenCtxDeleter>;
        GeoGenCtxPtr geometryContext{_geometryContext, [](GlmGeometryGenerationContext* ptr) {
                                         GLMC_FREE(ptr->_crowdFieldNames);
                                         GLMC_FREE(ptr->_cacheDirectories);
                                         GLMC_FREE(ptr->_characterFilesDirectories);
                                         GLMC_FREE(ptr->_excludedEntities);
                                         GLMC_FREE(ptr);
                                     }};

        auto beginStatus = glmBeginGeometryGeneration(_geometryContext);
        for (long index{}; index < _geometryContext->_entityCount; ++index)
        {
            using EntGeoDeleter = std::function<void(GlmEntityGeometry*)>;
            std::unique_ptr<GlmEntityGeometry, EntGeoDeleter> geo{(GlmEntityGeometry*)GLMC_MALLOC(sizeof(GlmEntityGeometry)),
                                                                  [](GlmEntityGeometry* ptr) {
                                                                      GLMC_FREE(ptr);
                                                                  }};

            GlmEntityInstanceMatrices instanceMatrices;
            auto geoStatus = glmCreateEntityGeometry(geo.get(), _geometryContext, &(_geometryContext->_entityBBoxes[index]), instanceMatrices);

            glmDestroyEntityGeometry(geo.get(), _geometryContext, &(_geometryContext->_entityBBoxes[index]));
        }
        auto endStatus = glmEndGeometryGeneration(_geometryContext);
    }
    std::cout << "Frame: " << frameNumber << " done." << std::endl;
}

int main(int, char**)
{
    glmInitCrowdIO();

    std::cout << "Geometry Generation Example" << std::endl;
    std::cout << "Directory: " << GOLAEM_TEST_CACHE_DIR << std::endl;
    std::cout << "Cache name: " << GOLAEM_TEST_CACHE_NAME << std::endl;
    std::cout << "Field name: " << GOLAEM_TEST_FIELD_NAME << std::endl;

    for (int i = 1002; i < 1100; ++i)
    {
        createGeometryGenerationContext(i);
    }

    glmFinishCrowdIO();
}