//
// Created by bareya on 05/12/18.
//

// this example is to check if memory is released through LD_PRELOAD

#include <vector>

#include <GolaemCache/GolaemCache.h>

#include "GolaemTestPath.h"

using namespace Golaem;

int main(int argc, char* argv[])
{
    CacheParameters parms;
    parms.setSimulationPath(GOLAEM_TEST_SIMULATION_PATH);
    parms.setLayoutPath(GOLAEM_TEST_LAYOUT_PATH);
    parms.appendCharacter(GOLAEM_TEST_CHAR_GCHA);

    for (int i = 1002; i < 1100; ++i)
    {
        std::vector<Entity> entities;
        GolaemCache::getEntities(entities, parms, EntityLOD::SkinDisplay, i);

        for (const Entity& e : entities)
        {
            e.getGeometryData();
//            e.getFrameData();
        }
    }

    return 0;
}