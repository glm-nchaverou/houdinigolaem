#ifndef GOLAEM_TEST_DATA_H
#define GOLAEM_TEST_DATA_H

// Simulation and Frame defines
#define GOLAEM_TEST_CACHE_DIR "/mnt/linux/cpp/HoudiniGolaem/test/cache"
#define GOLAEM_TEST_CACHE_NAME "golaem0001_crowd_golaem6_4_testPiotr_v0001"
#define GOLAEM_TEST_FIELD_NAME "crowdField1"
#define GOLAEM_TEST_PREFIX_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1."
#define GOLAEM_TEST_STREAM_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.%d.gscf"
#define GOLAEM_TEST_SIMULATION_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.gscs"
#define GOLAEM_TEST_FRAME1_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.1.gscf"
#define GOLAEM_TEST_FRAME2_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.2.gscf"
#define GOLAEM_TEST_FRAME3_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.3.gscf"
#define GOLAEM_TEST_FRAME4_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.4.gscf"
#define GOLAEM_TEST_TERRAIN_SOURCE_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem0001_crowd_golaem6_4_testPiotr_v0001.crowdField1.terrain.gtg"
#define GOLAEM_TEST_TERRAIN_DESTINATION_PATH "/mnt/linux/cpp/HoudiniGolaem/test/cache/golaem6_4_testPiotr_DEST_v0001.destination.terrain.gtg"
#define GOLAEM_TEST_LAYOUT_PATH "/mnt/linux/cpp/HoudiniGolaem/test/layout/golaem6_4_testPiotr_v0001.gscl"

// Geometry Generation Context defines
#define GOLAEM_TEST_CHAR_DIR "/mnt/linux/cpp/HoudiniGolaem/test/characters"
#define GOLAEM_TEST_CHAR_GCHA "/mnt/linux/cpp/HoudiniGolaem/test/characters/CasualMan_Light.gcha"
#define GOLAEM_TEST_CHAR_GCG "/mnt/linux/cpp/HoudiniGolaem/test/characters/CasualMan_Light.gcg"
#define GOLAEM_TEST_CHAR_APX "/mnt/linux/cpp/HoudiniGolaem/test/characters/CasualMan_Flag.apx"

#endif // GOLAEM_TEST_DATA_H
