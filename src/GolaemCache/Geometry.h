//
// Created by bareya on 29/11/18.
//

#ifndef GOLAEMCACHE_GEOMETRY_H
#define GOLAEMCACHE_GEOMETRY_H

#include <memory>
#include <vector>

#include "CacheParameters.h"
#include "Handles.h"

namespace Golaem
{

class CacheParameters;

class GeoContext : public std::enable_shared_from_this<GeoContext>
{
public:
    GeoContext(const CacheParameters& parms, long frame);
    GeoContext(const CacheParametersHandle& parms, long frame);

public:
    static GeoContextHandle create(const CacheParameters& parms, long frame)
    {
        return GeoContextHandle{new GeoContext{parms, frame}};
    }

    static GeoContextHandle  create(const CacheParametersHandle& parms, long frame)
    {
        return GeoContextHandle{new GeoContext{parms, frame}};
    }

    GeoContext(const GeoContext&) = delete;
    GeoContext(GeoContext&&) = delete;

    ~GeoContext() = default;

    GeoContext& operator=(const GeoContext&) = delete;
    GeoContext& operator=(GeoContext&&) = delete;

    GlmGeometryGenerationContext* getData() noexcept
    {
        return m_context.get();
    }

private:
    CacheParameters m_parms;

    CacheParametersHandle m_parmsHandle; // TODO use those parameters

    using GeoGenCtxDeleter = std::function<void(GlmGeometryGenerationContext*)>;
    using GeoGenCtxPtr = std::unique_ptr<GlmGeometryGenerationContext, GeoGenCtxDeleter>;

    GeoGenCtxPtr m_context;
};

///
///
///
class Geometry
{
    Geometry(const GeoContextHandle& context, long index);

public:
    static GeometryHandle create(const GeoContextHandle& context, long index)
    {
        return GeometryHandle{new Geometry(context, index)};
    }

    ~Geometry()
    {
    }

    const GlmEntityGeometry* getData();

    GeoContext* getContext() noexcept
    {
        return m_context.get();
    }

private:
    GeoContextHandle m_context;
    long m_index;

    using GeometryDeleter = std::function<void(GlmEntityGeometry*)>;
    using GeometryPtr = std::unique_ptr<GlmEntityGeometry, GeometryDeleter>;
    GeometryPtr m_geometry;
};

} // namespace Golaem

#endif // GOLAEMCACHE_GEOMETRY_H
