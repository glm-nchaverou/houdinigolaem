//
// Created by bareya on 29/11/18.
//

#ifndef GOLAEMCACHE_SIMULATION_H
#define GOLAEMCACHE_SIMULATION_H

#include "CacheParameters.h"
#include "Handles.h"

namespace Golaem
{

///
/// Wraps SimulationData into a smart pointer.
///
/// An instance of this class is not movable nor copyable.
/// Can be created on the heap only to avoid copying underlying
/// parameters and data.
///
///
class Simulation : public std::enable_shared_from_this<Simulation>
{
    Simulation(const CacheParameters& parms);

public:
    static SimulationHandle create(const CacheParameters& parms);

    Simulation(const Simulation&) = delete;
    Simulation(Simulation&&) = delete;

    ~Simulation();

    Simulation& operator=(const Simulation&) = delete;
    Simulation& operator=(Simulation&&) = delete;

    /// Parameters used to open current SimulationCache.
    const CacheParameters& parameters() const noexcept
    {
        return m_parms;
    }

    /// Depending on parms passed to the constructor might return simple or modified.
    GlmSimulationData* getData() noexcept
    {
        return getModifiedData() == nullptr ? getSimpleData() : getModifiedData();
    }

    /// Access or opens a new FrameData. This method can throw
    /// if frame is not found. Once frame is opened it stays opened
    /// until an instance of this object is destroyed.
    FrameHandle getFrame(long number);

    bool isModified() const
    {
        return m_modified != nullptr;
    }

    // During initialization needs to access Simple and Modified data.
    friend class Frame;

private:
    /// Returns pointer to simple data, can return nullptr.
    GlmSimulationData* getSimpleData() noexcept
    {
        return m_simple.get();
    }

    /// Returns pointer to modified data, can return nullptr.
    GlmSimulationData* getModifiedData() noexcept
    {
        return m_modified.get();
    }

    CacheParameters m_parms;

    using SimulationPtr = std::unique_ptr<GlmSimulationData, std::function<void(GlmSimulationData*)>>;
    SimulationPtr m_simple;

    class Layout;
    std::unique_ptr<Layout> m_layout;

    SimulationPtr m_modified;
};

///
/// Caches FrameData.
///
/// An instance of this class is not movable nor copyable.
///
/// It auto manages lifespan of underlying FrameData. Each FrameCache
/// is a child of SimulationCache, therefore frame, can't outlive a simulation.
///
///
class Frame
{
    Frame(const SimulationHandle& simulation, long frameNumber);

public:
    static FrameHandle create(const SimulationHandle& simulation, long frameNumber);

    Frame(const Frame&) = delete;
    Frame(Frame&&) = delete;

    ~Frame() = default;

    Frame& operator=(const Frame&) = delete;
    Frame& operator=(Frame&&) = delete;

    Simulation* getSimulationCache() noexcept
    {
        return m_simulation.get();
    }

    long number() const noexcept
    {
        return m_frameNumber;
    }

    GlmFrameData* getData() noexcept
    {
        return getModifiedData() == nullptr ? getSimpleData() : getModifiedData();
    }

    bool isModified() const
    {
        return m_modified != nullptr;
    }

private:
    GlmFrameData* getSimpleData() noexcept
    {
        return m_simple.get();
    }

    GlmFrameData* getModifiedData() noexcept
    {
        return m_modified.get();
    }

    SimulationHandle m_simulation;

    long m_frameNumber;

    using FrameDataDeleter = std::function<void(GlmFrameData* ptr)>;
    using FrameDataPtr = std::unique_ptr<GlmFrameData, FrameDataDeleter>;
    FrameDataPtr m_simple;
    FrameDataPtr m_modified;
};

inline bool operator==(const std::weak_ptr<Simulation>& lhs, const Golaem::CacheParameters& rhs)
{
    if (lhs.expired())
        return false;

    return lhs.lock()->parameters() == rhs;
}

inline bool operator==(const std::weak_ptr<Frame>& lhs, long value)
{
    if (lhs.expired())
        return false;

    return lhs.lock()->number() == value;
}

} // namespace Golaem

#endif //GOLAEMCACHE_SIMULATION_H
