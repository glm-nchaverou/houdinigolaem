//
// Created by bareya on 02/12/18.
//

#include "Entity.h"
#include "CacheParameters.h"
#include "Geometry.h"
#include "GolaemCache.h"
#include "Simulation.h"

#include <glm_crowd_io.h>

using namespace Golaem;

Entity::Entity(const CacheParameters& parms,
               const FrameHandle& frameCache,
               EntityLOD lod, long index)
{
    const GlmSimulationData* simData = frameCache->getSimulationCache()->getData();

    // check entity index correctness
    if (index < 0 || index >= simData->_entityCount)
    {
        throw std::runtime_error("Entity index out of range!");
    }

    // copy parameters
    m_newParms = parms;
    m_frame = frameCache;
    m_lod = lod;
    m_frameNumber = frameCache->number();
    m_index = index;
    m_id = simData->_entityIds[m_index];
    m_dirty = false;
}

Entity::Entity(const CacheParameters& parms,
               const GeometryHandle& geometry,
               EntityLOD lod, long index)
{
    auto context = geometry->getContext()->getData();
    m_lod = lod;
    m_frameNumber = static_cast<long>(context->_frame);
    m_index = index;
    m_id = context->_entityBBoxes[index]._entityId;
    m_geometry = geometry;
    m_dirty = false;
}

const GlmSimulationData* Entity::getSimulationData() const
{
    if (m_dirty)
    {
        updateSimulationFrameCache();
    }

    return m_frame->getSimulationCache()->getData();
}

const GlmFrameData* Entity::getFrameData() const
{
    if (m_dirty)
    {
        updateSimulationFrameCache();
    }

    return m_frame->getData();
}

Entity::~Entity()
{
}

const GlmEntityGeometry *Entity::getGeometryData() const {
    if (m_dirty)
    {
        updateGeometryGenerationCache();
    }

    return m_geometry->getData();
}
