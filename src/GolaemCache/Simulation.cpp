//
// Created by bareya on 29/11/18.
//

#include "Simulation.h"
#include "GolaemCache.h"

#include <glm_crowd_io.h>

#include <iostream>

using namespace Golaem;

namespace Golaem
{
///
/// Aggregates all data needed to create Layout.
///
/// This class is used internally by SimulationCache.
/// It contains History, Terrain, EntityTransforms.
///
class Simulation::Layout
{
public:
    Layout() = default;

    Layout(const Simulation& simulation);

    using HistoryPtr = std::unique_ptr<GlmHistory, std::function<void(GlmHistory*)>>;
    HistoryPtr m_history;

    int m_transformCount{};
    std::unique_ptr<GlmEntityTransform, std::function<void(GlmEntityTransform*)>> m_transforms;

    TerrainHandle m_terrainSource;
    TerrainHandle m_terrainDestination;
};

} // namespace Golaem

Simulation::Layout::Layout(const Simulation& simulation)
{
    //
    const CacheParameters& parms = simulation.parameters();
    GlmSimulationData* simpleData = simulation.m_simple.get();
    assert(simpleData != nullptr);

    // Create new history object
    GlmHistory* history;
    auto status = glmCreateAndReadHistoryJSON(&history, parms.getLayoutPath().c_str());
    if (status != GSC_SUCCESS)
    {
        throw std::runtime_error("Can't open history file.");
    }
    HistoryPtr historyPtr{history, [](GlmHistory* ptr) {
                              glmDestroyHistory(&ptr);
                          }};

    auto terrainSourcePath = parms.getTerrainSource();
    m_terrainSource = Golaem::GolaemCache::getTerrain(terrainSourcePath.c_str(), false /*throw*/);
    history->_terrainMeshSource = m_terrainSource.get();

    if (!parms.getTerrainDestination().empty())
    {
        auto terrainDestinationPath = parms.getTerrainDestination();
        m_terrainDestination = Golaem::GolaemCache::getTerrain(terrainDestinationPath.c_str(), true /*throw*/);
        history->_terrainMeshDestination = m_terrainDestination.get();
    }

    // Create entity transforms.
    GlmEntityTransform* transforms{};
    int numTransforms{};
    glmCreateEntityTransforms(simpleData, history, &transforms, &numTransforms);
    auto TransformsDeleter = [numTransforms](GlmEntityTransform* ptr) {
        glmDestroyEntityTransforms(&ptr, static_cast<unsigned int>(numTransforms));
    };
    m_transforms = std::unique_ptr<GlmEntityTransform, std::function<void(GlmEntityTransform*)>>(transforms, TransformsDeleter);
    m_transformCount = numTransforms;

    // everything is properly constructed, store as attributes
    m_history = std::move(historyPtr);
}

Simulation::Simulation(const CacheParameters& parms)
{
    // try to open simulation and store it here.
    GlmSimulationData* simpleSimulationData;
    auto status = glmCreateAndReadSimulationData(&simpleSimulationData, parms.getSimulationPath().c_str());
    if (status != GSC_SUCCESS)
    {
        throw std::runtime_error("Failed to open simulation data.");
    }

    m_simple = SimulationPtr(simpleSimulationData, [](GlmSimulationData* ptr) {
        glmDestroySimulationData(&ptr);
    });

    // Simulation is initialized properly, copy parameters
    m_parms = parms;

    // check if modified, create layout, create modified simpleSimulationData
    if (!parms.getLayoutPath().empty())
    {
        m_layout = std::make_unique<Layout>(*this);

        GlmSimulationData* modifiedSimulationData;
        glmCreateModifiedSimulationData(simpleSimulationData,
                m_layout->m_transforms.get(),
                m_layout->m_transformCount,
                &modifiedSimulationData);

        m_modified = SimulationPtr(modifiedSimulationData, [](GlmSimulationData* ptr) {
            glmDestroySimulationData(&ptr);
        });
    }
}

SimulationHandle Simulation::create(const CacheParameters& parms)
{
    return SimulationHandle{new Simulation{parms}};
}

Simulation::~Simulation()
{
}

FrameHandle Frame::create(const SimulationHandle& simulation, long frameNumber)
{
    return FrameHandle{new Frame{simulation, frameNumber}};
}

Frame::Frame(const SimulationHandle& simulation, long frameNumber)
{
    assert(simulation.get() != nullptr);

    const CacheParameters& parms = simulation->parameters();

    GlmFrameData* simpleFrameData;
    GlmSimulationData* simpleSimulationData = simulation->getSimpleData();
    GlmSimulationData* modifiedSimulationData = simulation->getModifiedData();

    // create simple frame data
    glmCreateFrameData(&simpleFrameData, simpleSimulationData);
    m_simple = FrameDataPtr(simpleFrameData, [simpleSimulationData](GlmFrameData* ptr) {
        glmDestroyFrameData(&ptr, simpleSimulationData);
    });



    auto framePath = parms.getFramePath(frameNumber);
    if (glmReadFrameData(simpleFrameData, simulation->getSimpleData(), framePath.c_str()) != GSC_SUCCESS)
    {
        throw std::runtime_error(std::string("Can't read simpleFrameData frame data ") + framePath.c_str());
    }

    if(simulation->isModified())
    {
        // create modified frame data
        GlmFrameData* modifiedFrameData;
        if(glmCreateModifiedFrameData(simpleSimulationData, simpleFrameData,
                                   simulation->m_layout->m_transforms.get(),
                                   simulation->m_layout->m_transformCount,
                                   simulation->m_layout->m_history.get(),
                                   modifiedSimulationData, &modifiedFrameData,
                                   frameNumber,
                                   parms.getStream().c_str(),
                                   parms.getDirectory().c_str()) != GSC_SUCCESS)
        {
            throw std::runtime_error(std::string("Can't read modifiedFrameData frame data ") + framePath.c_str());
        }

        m_modified = FrameDataPtr(modifiedFrameData, [modifiedSimulationData](GlmFrameData* ptr) {
            glmDestroyFrameData(&ptr, modifiedSimulationData);
        });
    }

    // Frame is constructed then copy handle
    m_simulation = simulation;
    m_frameNumber = frameNumber;
}

FrameHandle Simulation::getFrame(long number)
{
    return Frame::create(shared_from_this(), number);
}
