//
// Created by bareya on 22/11/18.
//

#ifndef GOLAEMCACHE_H
#define GOLAEMCACHE_H

#include "CacheParameters.h"
#include "Entity.h"

#include <list>
#include <map>
#include <vector>

namespace Golaem
{

///
/// Keeps track of all caches.
///`
class GolaemCache
{
    GolaemCache();

public:
    static GolaemCache& getInstance();

    GolaemCache(const GolaemCache&) = delete;
    GolaemCache(GolaemCache&&) = delete;

    ~GolaemCache();

    GolaemCache& operator=(const GolaemCache&) = delete;
    GolaemCache& operator=(GolaemCache&&) = delete;

    static void getEntities(std::vector<Entity>& entities,
                            const CacheParameters& parms,
                            EntityLOD lod = EntityLOD::Box,
                            long frameNumber = 1);

    /// An instance of SimulationCache
    static SimulationHandle getSimulation(const CacheParameters &parms);
    static FrameHandle getFrame(const CacheParameters &parms, long number);

    /// Global cache for all terrain files.
    static TerrainHandle getTerrain(const char* path, bool throws = true);

    /// access cached Geometry
    static GeoContextHandle getGeoContext(const CacheParameters &eParms, long frame);

private:
    std::map<std::string, std::weak_ptr<CrowdTerrain::Mesh>> m_terrains;
    std::list<std::weak_ptr<Simulation>> m_simulations;

    // TODO: append to the list to extend lifetime.
    std::list<FrameHandle> m_persistedFrames;
};

} // namespace Golaem

#endif // GOLAEMCACHE_H
