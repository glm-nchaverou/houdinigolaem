//
// Created by bareya on 28/11/18.
//

#ifndef HOUDINIGOLAEM_CACHEPARAMETERS_H
#define HOUDINIGOLAEM_CACHEPARAMETERS_H

//#include <UT/UT_String.h>
//#include <UT/UT_StringArray.h>
//
//#include <UT/UT_WorkArgs.h>

#include <cassert>
#include <memory>
#include <vector>

namespace Golaem
{

class Simulation;
class Frame;

// Holds all strings needed by
class CacheParameters
{
public:
    using value_type = std::string;
    using array_type = std::vector<std::string>;

    CacheParameters() = default;

    explicit CacheParameters(const CacheParameters&) = default;
    CacheParameters(CacheParameters&&) = default;

    CacheParameters& operator=(const CacheParameters&) = default;
    CacheParameters& operator=(CacheParameters&&) = default;

    // setters

    void setSimulationPath(const char* path)
    {
        m_simulation = path;
    }

    void setSimulationPath(value_type && path)
    {
        m_simulation = std::move(path);
    }

    void setLayoutPath(const char* path)
    {
        m_layout = path;
    }

    void setLayoutPath(value_type && path)
    {
        m_layout = std::move(path);
    }

    void setTerrainDestination(const char* path)
    {
        m_terrainDestination = path;
    }

    void setTerrainDestination(value_type && path)
    {
        m_terrainDestination = std::move(path);
    }

    void appendCharacter(const char* path)
    {
        m_characters.emplace_back(path);
    }

    // getters

    const value_type& getSimulationPath() const
    {
        return m_simulation;
    }

    const value_type& getLayoutPath() const
    {
        return m_layout;
    }

    const value_type& getTerrainDestination() const
    {
        return m_terrainDestination;
    }

    const array_type& getCharacters() const
    {
        return m_characters;
    }

    value_type getFramePath(long frame) const
    {
        return replaceExtension(m_simulation, std::to_string(frame)+".gscf");
    }

    bool operator==(const CacheParameters& other) const
    {
        return m_simulation == other.m_simulation &&
               m_layout == other.m_layout &&
               m_terrainDestination == other.m_terrainDestination &&
               m_characters == other.m_characters;
    }

    const value_type getTerrainSource() const
    {
        // TODO cache
        return replaceExtension(m_simulation, "terrain.gtg");
    }

    value_type getDirectory() const
    {
        // TODO cache
        auto found = m_simulation.rfind('/');
        return found != std::string::npos ? m_simulation.substr(0, found) : m_simulation;
    }

    value_type getStream() const
    {
        return replaceExtension(m_simulation, "%d.gscf");
    }

    void getSimulationComponents(value_type &cacheName, value_type &fieldName) const
    {
        auto fileFound = m_simulation.rfind('/');
        fileFound = fileFound != std::string::npos ? fileFound : 0;

        auto cacheFound = m_simulation.find('.', fileFound);
        auto fieldFound = m_simulation.find('.', cacheFound+1);
        auto endFound = m_simulation.find('.', fieldFound+1);

        if(cacheFound != std::string::npos && fieldFound != std::string::npos && endFound == std::string::npos)
        {
            cacheName = m_simulation.substr(fileFound+1, cacheFound-fileFound-1);
            fieldName = m_simulation.substr(cacheFound+1, fieldFound-cacheFound-1);
            return;
        }

        throw std::runtime_error("Invalid simulation file path, can't tokenize cache name and field name");
    }

    static value_type replaceExtension(const value_type& path, const value_type& with)
    {
        auto found = path.rfind('.');
        if(found != std::string::npos)
        {
            value_type result;
            result.reserve(found + with.size());
            return path.substr(0,found+1) + with;
        }
        else
        {
            return path;
        }
    }

private:
    value_type m_simulation;
    value_type m_layout;
    value_type m_terrainDestination;
    array_type m_characters;
};

} // namespace Golaem

#endif //HOUDINIGOLAEM_CACHEPARAMETERS_H
