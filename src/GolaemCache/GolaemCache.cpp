//
// Created by bareya on 22/11/18.
//

#include "Simulation.h"

#include "Geometry.h"
#include "GolaemCache.h"

#include <glm_crowd_io.h>

#include <algorithm>
#include <iostream>

using namespace Golaem;

void GolaemCache::getEntities(std::vector<Entity>& entities, const CacheParameters& parms, EntityLOD lod, long frameNumber)
{
    // must initialize instance
    getInstance();

    switch (lod)
    {
    case EntityLOD::Box:
    case EntityLOD::Skeleton:
    {
        auto sim = getSimulation(parms);
        auto frame = sim->getFrame(frameNumber);

        for (long index{}; index < sim->getData()->_entityCount; ++index)
        {
            entities.emplace_back(parms, frame, lod, index);
        }

        break;
    }
    case EntityLOD::SkinDisplay:
    {
        auto context = getGeoContext(parms, frameNumber);

        for (long index{}; index < context->getData()->_entityCount; ++index)
        {
            auto geometry = Geometry::create(context, index);
            entities.emplace_back(parms, geometry, lod, index);
        }
        break;
    }
    }
}

SimulationHandle GolaemCache::getSimulation(const CacheParameters& parms)
{
    GolaemCache& golaemCache = getInstance();

    auto found = std::find(std::begin(golaemCache.m_simulations), std::end(golaemCache.m_simulations), parms);

    if (found != std::end(golaemCache.m_simulations)) // if found check if it hasn't expired
    {
        if (!found->expired())
        {
            return found->lock();
        }
        else
        {
            golaemCache.m_simulations.erase(found);
        }
    }

    SimulationHandle simulationPtr = Simulation::create(parms);

    golaemCache.m_simulations.push_back(simulationPtr);
    return simulationPtr;
}

FrameHandle GolaemCache::getFrame(const CacheParameters& parms, long number)
{
    SimulationHandle simulation = getInstance().getSimulation(parms);
    return simulation->getFrame(number);
}

TerrainHandle GolaemCache::getTerrain(const char* path, bool throws)
{
    GolaemCache& golaemCache = getInstance();

    // find terrain in the cache
    auto found = golaemCache.m_terrains.find(path);
    if (found != std::end(golaemCache.m_terrains)) // if found check if it hasn't expired
    {
        if (!found->second.expired())
        {
            return found->second.lock();
        }
        else
        {
            golaemCache.m_terrains.erase(found);
        }
    }

    // Cache does not exist or it has expired, open new one, and replace the expired cache
    CrowdTerrain::Mesh* terrain = CrowdTerrain::loadTerrainAsset(path);
    if (!terrain)
    {
        if (throws)
            throw std::runtime_error("Terrain can't be opened");
        else
            return nullptr;
    }

    // create shared pointer that we are going to return
    auto terrainPtr = TerrainHandle{
        terrain, [](CrowdTerrain::Mesh* ptr) {
            CrowdTerrain::closeTerrainAsset(ptr);
        }};

    // update weak reference.
    golaemCache.m_terrains.emplace(path, terrainPtr);

    return terrainPtr;
}

GeoContextHandle GolaemCache::getGeoContext(const CacheParameters& parms, long frame)
{
    // TODO: implement cache
    auto context = GeoContext::create(parms, frame);
    return context;
}

GolaemCache& GolaemCache::getInstance()
{
    // Do not change it to pointer that is not deleted.
    // We rely on proper destruction.
    static GolaemCache golaemCache;
    return golaemCache;
}

GolaemCache::GolaemCache()
{
    // GolaemCrowd must be initialized, if not,
    // geometry generation context seg faults.
    // Since GolaemCache is a singleton, this is
    // the right place to do it.
    glmInitCrowdIO();

    // Must be set in order to have
    // Layout Terrain Adaptation to work
    glmRaycastClosest = RaycastClosest;
    glmTerrainSetFrame = TerrainSetFrame;
}

GolaemCache::~GolaemCache()
{
    glmFinishCrowdIO();
}


