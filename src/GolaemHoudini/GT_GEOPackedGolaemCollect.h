//
// Created by bareya on 26/11/18.
//

#ifndef HOUDINIGOLAEM_GT_GEOPACKEDGOLAEM_H
#define HOUDINIGOLAEM_GT_GEOPACKEDGOLAEM_H

#include <GT/GT_GEOPrimCollect.h>

class GT_GEOPackedGolaemCollect : public GT_GEOPrimCollect
{
public:
    static void registerPrimitive(const GA_PrimitiveTypeId& id);

    GT_GEOPackedGolaemCollect(const GA_PrimitiveTypeId& id);

    GT_GEOPrimCollectData* beginCollecting(const GT_GEODetailListHandle& geometry,
                                           const GT_RefineParms* parms) const override;

    GT_PrimitiveHandle collect(const GT_GEODetailListHandle& geometry,
                               const GEO_Primitive* const* primList,
                               int nSegments,
                               GT_GEOPrimCollectData* data) const override;

    GT_PrimitiveHandle endCollecting(const GT_GEODetailListHandle& geometry,
                                     GT_GEOPrimCollectData* data) const override;
};

#endif //HOUDINIGOLAEM_GT_GEOPACKEDGOLAEM_H
