//
// Created by bareya on 26/11/18.
//

#include "GT_GEOPackedGolaemCollect.h"

#include "GU_PackedGolaem.h"

#include <GolaemCache/Geometry.h>
#include <GolaemCache/Simulation.h>

#include <GT/GT_PrimCircle.h>

#include <GT/GT_PrimCollect.h>
#include <GT/GT_PrimCurveMesh.h>
#include <GT/GT_PrimPointMesh.h>
#include <GT/GT_PrimPolygon.h>
#include <GT/GT_PrimPolygonMesh.h>

#include <GT/GT_DAValues.h>

#include <iostream>

#include <glmHierarchicalBone.h>
#include <glm_crowd_io.h>

GT_PrimitiveHandle drawSkeletonAsLines(const Golaem::Entity& entityParams)
{
    // collect cache.
    auto simulationData = entityParams.getSimulationData();
    auto frameData = entityParams.getFrameData();

    exint iEntity = entityParams.getIndex();
    unsigned int entityType = simulationData->_entityTypes[iEntity];

    size_t mb_segs = 1;

    auto gtVtxCount = new GT_Int32Array(simulationData->_boneCount[entityType], 1);
    for (int i = 0; i < simulationData->_boneCount[entityType]; ++i)
    {
        gtVtxCount[0].data()[i] = 2; // two per bone
    }

    int vtxCount = simulationData->_boneCount[entityType];

    UT_StackBuffer<GT_DataArrayHandle> P{mb_segs};
    UT_StackBuffer<GT_Real32Array*> PData{mb_segs};

    for (int i = 0; i < mb_segs; ++i)
    {
        PData[i] = new GT_Real32Array(2 * vtxCount, 3, GT_TYPE_POINT);
        P[i] = PData[i];
    }

    for (exint i = 0; i < (vtxCount); ++i)
    {
        exint iBoneIndex = i + simulationData->_iBoneOffsetPerEntityType[entityType] + simulationData->_indexInEntityType[iEntity] * simulationData->_boneCount[entityType];
        exint iSnsIndex = i + simulationData->_snsOffsetPerEntityType[entityType] + simulationData->_indexInEntityType[iEntity] * simulationData->_snsCountPerEntityType[entityType];

        PData[0]->data()[i * 6 + 0] = frameData->_bonePositions[iBoneIndex][0];
        PData[0]->data()[i * 6 + 1] = frameData->_bonePositions[iBoneIndex][1];
        PData[0]->data()[i * 6 + 2] = frameData->_bonePositions[iBoneIndex][2];

        glm::Vector3 r;
        glm::Vector3 x{0, 0.1, 0};
        glm::Quaternion q{frameData->_boneOrientations[iBoneIndex][0],
                          frameData->_boneOrientations[iBoneIndex][1],
                          frameData->_boneOrientations[iBoneIndex][2],
                          frameData->_boneOrientations[iBoneIndex][3]};
        glm::Quaternion::mul(r, q, x);

        PData[0]->data()[i * 6 + 3] = frameData->_bonePositions[iBoneIndex][0] + r.x * frameData->_snsValues[iSnsIndex][0];
        PData[0]->data()[i * 6 + 4] = frameData->_bonePositions[iBoneIndex][1] + r.y * frameData->_snsValues[iSnsIndex][1];
        PData[0]->data()[i * 6 + 5] = frameData->_bonePositions[iBoneIndex][2] + r.z * frameData->_snsValues[iSnsIndex][2];
    }

    // create attributes
    GT_AttributeMap* aMap;

    aMap = new GT_AttributeMap();
    aMap->add("P", true); // you must add it first, otherwise it crashes.
    GT_AttributeList* points = new GT_AttributeList(aMap);

    for (int seg = 0; seg < mb_segs; ++seg)
    {
        points->set(0, P[seg], seg);
    }

    aMap = new GT_AttributeMap();
    GT_AttributeList* uniform = new GT_AttributeList(aMap);

    aMap = new GT_AttributeMap();
    GT_AttributeList* detail = new GT_AttributeList(aMap);

    return new GT_PrimCurveMesh(GT_BASIS_LINEAR, gtVtxCount,
                                GT_AttributeListHandle(points),
                                GT_AttributeListHandle(uniform),
                                GT_AttributeListHandle(detail),
                                false);
}

GT_PrimitiveHandle drawSkeletonAsPoints(const Golaem::Entity& entityParams)
{
    // collect cache.
    auto simulationData = entityParams.getSimulationData();
    auto frameData = entityParams.getFrameData();

    exint iEntity = entityParams.getIndex();
    unsigned int entityType = simulationData->_entityTypes[iEntity];

    size_t mb_segs = 1;
    int npts = simulationData->_boneCount[entityType];

    UT_StackBuffer<GT_DataArrayHandle> P{mb_segs};
    UT_StackBuffer<GT_Real32Array*> PData{mb_segs};

    for (int i = 0; i < mb_segs; ++i)
    {
        PData[i] = new GT_Real32Array(npts, 3, GT_TYPE_POINT);
        P[i] = PData[i];
    }

    for (exint i = 0; i < npts; ++i)
    {
        exint iBoneIndex = i + simulationData->_iBoneOffsetPerEntityType[entityType] + simulationData->_indexInEntityType[iEntity] * simulationData->_boneCount[entityType];
        PData[0]->data()[i * 3 + 0] = frameData->_bonePositions[iBoneIndex][0];
        PData[0]->data()[i * 3 + 1] = frameData->_bonePositions[iBoneIndex][1];
        PData[0]->data()[i * 3 + 2] = frameData->_bonePositions[iBoneIndex][2];
    }

    // create attributes
    GT_AttributeMap* aMap;

    aMap = new GT_AttributeMap();
    aMap->add("P", true); // you must add it first, otherwise it crashes.
    GT_AttributeList* points = new GT_AttributeList(aMap);

    for (int seg = 0; seg < mb_segs; ++seg)
    {
        points->set(0, P[seg], seg);
    }

    aMap = new GT_AttributeMap();
    GT_AttributeList* uniform = new GT_AttributeList(aMap);

    return {new GT_PrimPointMesh(GT_AttributeListHandle(points), GT_AttributeListHandle(uniform))};
}

class GT_SkeletonBonePositionsDA : public GT_DataArray
{
    float (*m_bonePositions)[3];
    exint m_boneCount;

public:
    GT_SkeletonBonePositionsDA(float (*bonePositions)[3], exint boneCount)
        : m_bonePositions{bonePositions}
        , m_boneCount{boneCount}
    {
    }

    const char* className() const override
    {
        return "GT_SkeletonBonePositionsDA";
    }

    GT_Storage getStorage() const override
    {
        return GT_STORE_REAL32;
    }

    GT_Size entries() const override
    {
        return m_boneCount;
    }

    GT_Size getTupleSize() const override
    {
        return 3;
    }

    int64 getMemoryUsage() const override
    {
        return 0;
    }

    uint8 getU8(GT_Offset offset, int idx) const override
    {
        return 0;
    }

    int32 getI32(GT_Offset offset, int idx) const override
    {
        return 0;
    }

    fpreal32 getF32(GT_Offset offset, int idx) const override
    {
        return m_bonePositions[offset][idx];
    }

    GT_String getS(GT_Offset offset, int idx) const override
    {
        return nullptr;
    }

    GT_Size getStringIndexCount() const override
    {
        return -1;
    }

    GT_Offset getStringIndex(GT_Offset offset, int idx) const override
    {
        return -1;
    }

    void getIndexedStrings(UT_StringArray& strings, UT_IntArray& indices) const override
    {
    }
};

GT_DataArrayHandle reverseMeshWinding(const GlmMesh* mesh, const GT_CountArray& vtxCounts)
{
    // Houdini uses left handed vertex winding, we need to reverse it, this is one unfortunate allocation.
    auto verticesPerPolygon = new GT_DANumeric<uint32_t>(mesh->_vertexIndicesPerPolygon, vtxCounts.sumCounts(), 1);
    for (auto polyIndex = 0UL, firstVertexOffset = 0UL; polyIndex < mesh->_polygonCount; firstVertexOffset += mesh->_verticePerPolygonCount[polyIndex], ++polyIndex)
    {
        for (auto vtxIndex = 0UL; vtxIndex < mesh->_verticePerPolygonCount[polyIndex]; ++vtxIndex)
        {
            unsigned long int reversedVtxOffset = firstVertexOffset + mesh->_verticePerPolygonCount[polyIndex] - vtxIndex - 1;
            verticesPerPolygon->set(mesh->_vertexIndicesPerPolygon[reversedVtxOffset], firstVertexOffset + vtxIndex);
        }
    }
    return verticesPerPolygon;
}

GT_PrimitiveHandle createSkinNoCopy(const GU_PackedGolaem* packed)
{
    const Golaem::Entity& entity = packed->getEntityParameters();
    auto geometry = entity.getGeometryData();

    GT_PrimCollect* polyCollection = new GT_PrimCollect();

    for (auto meshIndex = 0UL, pointNumberOffset = 0UL; meshIndex < geometry->_meshCount; ++meshIndex)
    {
        const GlmMesh* mesh = &(geometry->_meshes[meshIndex]);
        assert(mesh!=nullptr);

        // Create and set attributes
        GT_AttributeMap* aMap;

        // point
        aMap = new GT_AttributeMap();
        aMap->add("P", true); // you must add it first, otherwise it crashes.
//        aMap->add("__point_id", true);

        GT_AttributeList* shared = new GT_AttributeList(aMap);

        GT_DataArrayHandle P(new GT_SkeletonBonePositionsDA(mesh->_vertices[0], mesh->_verticeCount));
        shared->set(0, P, 0);

//        GT_DataArrayHandle point_id{new GT_DARange{0, mesh->_verticeCount}};
//        shared->set(1, point_id, 0);

        // vertices
        aMap = new GT_AttributeMap();
        GT_AttributeList* vertex = new GT_AttributeList(aMap);

        // primitives
        aMap = new GT_AttributeMap();
        GT_AttributeList* uniform = new GT_AttributeList(aMap);

        // detail
        aMap = new GT_AttributeMap();
        GT_AttributeList* detail = new GT_AttributeList(aMap);

        // polygon vertex count
        GT_DataArrayHandle polygonVertexCount{new GT_DAValues<uint32_t>(mesh->_verticePerPolygonCount, mesh->_polygonCount, 1)};
        GT_CountArray vtxCounts{polygonVertexCount};
        GT_DataArrayHandle verticesPerPolygonHandle{reverseMeshWinding(mesh, vtxCounts)};

        // Create primitive and append it to the collection
        GT_PrimitiveHandle meshHandle{new GT_PrimPolygonMesh{vtxCounts, verticesPerPolygonHandle, shared, vertex, uniform, detail}};
        polyCollection->appendPrimitive(meshHandle);

        pointNumberOffset += mesh->_verticeCount;
    }

    return polyCollection;
}

GT_PrimitiveHandle createSkeletonNoCopy(const Golaem::Entity& entityParams)
{
    // collect cache.
    auto simulationData = entityParams.getSimulationData();
    auto frameData = entityParams.getFrameData();

    // get entity details
    const exint iEntity = entityParams.getIndex();
    const exint entityType = simulationData->_entityTypes[iEntity];

    const exint boneCount = simulationData->_boneCount[entityType];
    const exint positionOffset = simulationData->_iBoneOffsetPerEntityType[entityType] + simulationData->_indexInEntityType[iEntity] * simulationData->_boneCount[entityType];

    GT_DataArrayHandle P(new GT_SkeletonBonePositionsDA(frameData->_bonePositions + positionOffset, boneCount));

    // create attributes
    GT_AttributeMap* aMap;

    aMap = new GT_AttributeMap();
    aMap->add("P", true); // you must add it first, otherwise it crashes.
    GT_AttributeList* points = new GT_AttributeList(aMap);
    points->set(0, P, 0);

    aMap = new GT_AttributeMap();
    GT_AttributeList* uniform = new GT_AttributeList(aMap);

    return {new GT_PrimPointMesh(GT_AttributeListHandle(points), GT_AttributeListHandle(uniform))};
}

namespace
{

class CollectData : public GT_GEOPrimCollectData
{
public:
    explicit CollectData(GT_GEODetailListHandle geometry)
        : GT_GEOPrimCollectData()
        , m_geometry{std::move(geometry)}
    {
    }

    const GT_GEODetailListHandle m_geometry;
};

} // namespace

void GT_GEOPackedGolaemCollect::registerPrimitive(const GA_PrimitiveTypeId& id)
{
    new GT_GEOPackedGolaemCollect(id);
}

GT_GEOPackedGolaemCollect::GT_GEOPackedGolaemCollect(const GA_PrimitiveTypeId& id)
{
    bind(id);
}

GT_GEOPrimCollectData* GT_GEOPackedGolaemCollect::beginCollecting(const GT_GEODetailListHandle& geometry,
                                                                  const GT_RefineParms* parms) const
{
    return new CollectData(geometry);
}

GT_PrimitiveHandle GT_GEOPackedGolaemCollect::collect(const GT_GEODetailListHandle& geometry,
                                                      const GEO_Primitive* const* primList,
                                                      int nSegments,
                                                      GT_GEOPrimCollectData* data) const
{
    // cast collected data check for primitive;
    auto collector = data->asPointer<CollectData>();

    auto packed = UTverify_cast<const GU_PrimPacked*>(primList[0]);
    auto impl = UTverify_cast<const GU_PackedGolaem*>(packed->implementation());

    // do not draw invalid primitives
    if (!impl->isValid())
    {
        return GT_PrimitiveHandle();
    }

    const Golaem::Entity& entityParams = impl->getEntityParameters();

    switch (entityParams.lod())
    {
    case Golaem::EntityLOD::Box:
    {
        // pass entity cache
        return {new GT_PrimCircle()};
    }
    case Golaem::EntityLOD::Skeleton:
    {
        return createSkeletonNoCopy(entityParams);
    }
    case Golaem::EntityLOD::SkinDisplay:
    {
        return createSkinNoCopy(impl);
    }
    }

    return {};
}

GT_PrimitiveHandle GT_GEOPackedGolaemCollect::endCollecting(const GT_GEODetailListHandle& geometry,
                                                            GT_GEOPrimCollectData* data) const
{
    auto collector = data->asPointer<CollectData>();
    //delete collector;
    return GT_PrimitiveHandle();
}
