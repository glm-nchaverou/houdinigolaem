//
// Created by bareya on 25/11/18.
//

#include "SOP_GolaemImporter.h"
#include "GU_PackedGolaem.h"

#include <GolaemCache/GolaemCache.h>

using Golaem::CacheParameters;
using Golaem::EntityLOD;
using Golaem::GolaemCache;

#include <GU/GU_PrimPacked.h>
#include <OP/OP_AutoLockInputs.h>
#include <PRM/PRM_TemplateBuilder.h>
#include <UT/UT_StringHolder.h>

const UT_StringHolder SOP_GolaemImporter::theSOPTypeName{"golaemimporter"};

/// This is a multi-line raw string specifying the parameter interface
/// for this SOP.
static const char* theDsFile = R"THEDSFILE(
{
    name        parameters
    parm {
        name    "simulation"
        label   "Simulation"
        type    file
        default { "" }
        parmtag { "filechooser_mode" "read" }
        parmtag { "filechooser_pattern" "*.gscs" }
        help    "A full path to the simulation file."
        export  all
    }
    parm {
        name "uselayout"
        label "Use Layout"
        type toggle
        nolabel
        joinnext
        default { "0" }
        help "Toggle to turn on/off layout file usage."
    }
    parm {
        name "layout"
        label "Layout"
        type file
        default { "" }
        disablewhen "{ uselayout == 0 }"
        parmtag { "filechooser_mode" "read" }
        parmtag { "filechooser_pattern" "*.gscl" }
        help "A full path to layout/history file."
        export  all
    }
    parm {
        name "useterrain"
        label "Use Terrain"
        type toggle
        nolabel
        joinnext
        default { "0" }
        help "Toggle to turn on/off destination terrain file usage."
    }
    parm {
        name "terrain"
        label "Terrain"
        type file
        default { "" }
        disablewhen "{ useterrain == 0 }"
        parmtag { "filechooser_mode" "read" }
        parmtag { "filechooser_pattern" "*.gtg" }
        help    "A full path to destination terrain file."
        export  all
    }
    parm {
        name "usecharacters"
        label "Use Characters"
        type toggle
        nolabel
        joinnext
        default { "0" }
    }
    multiparm {
        name "characters"
        label "Characters"
        parm {
            name "character#"
            label "Character #"
            type string
            default { "" }
        }
        disablewhen "{ usecharacters == 0 }"
    }
    parm {
        name "display"
        label "Display"
        type integer
        default { "0" }
        menu {
            "0" "BBox"
            "1" "Skeleton"
            "2" "SkinDisplay"
        }
        range { 0 2 }
        help "Pick display method"
    }
    parm {
        name "frame"
        label "Frame"
        default { "$FF" }
        range { 0 240 }
        help "Simulation frame to open."
    }
}
)THEDSFILE";

PRM_Template* SOP_GolaemImporter::buildTemplates()
{
    static PRM_TemplateBuilder templates("SOP_GolaemImporter.cpp", theDsFile);
    return templates.templates();
}

SOP_GolaemImporter::SOP_GolaemImporter(OP_Network* net, const char* name, OP_Operator* op)
    : SOP_Node(net, name, op)
{
    //mySopFlags.setManagesDataIDs(true);
}

OP_ERROR SOP_GolaemImporter::cookMySop(OP_Context& context)
{
    OP_AutoLockInputs lock(this);
    if (lock.lock(context) >= UT_ERROR_ABORT)
    {
        return error();
    }

    // cleanup
    gdp->clearAndDestroy();

    auto time = context.getTime();


    CacheParameters parms;

    UT_String simulationPath;
    evalString(simulationPath, "simulation", 0, time);
    parms.setSimulationPath(std::move(simulationPath));

    // layout parm
    if (evalInt("uselayout", 0, time))
    {
        UT_String layoutPath;
        evalString(layoutPath, "layout", 0, time);
        parms.setLayoutPath(std::move(layoutPath));
    }

    // terrain parm
    if (evalInt("useterrain", 0, time))
    {

        UT_String terrainDestinationPath;
        evalString(terrainDestinationPath, "terrain", 0, time);
        parms.setTerrainDestination(std::move(terrainDestinationPath));
    }

    if (evalInt("usecharacters", 0, time))
    {
        for (int i = 1; i <= evalInt("characters", 0, 0.0f); ++i)
        {
            UT_String value;
            evalStringInst("character#", &i, value, 0, time);
            parms.appendCharacter(value.c_str());
        }
    }

    //
    auto displayLod = evalInt("display", 0, time);
    auto frame = evalInt("frame", 0, time);

    // create GolaemPacked
    try
    {
        bool containsDegenerate{false};

        std::vector<Golaem::Entity> entities;
        GolaemCache::getEntities(entities, parms, static_cast<EntityLOD>(displayLod), frame);
        for (auto it = entities.begin(); it != entities.end(); ++it)
        {
            auto packed = GU_PackedGolaem::build(*gdp, std::move(*it));
            containsDegenerate = packed->isDegenerate() ? true : containsDegenerate;
        }

        if (containsDegenerate)
        {
            addWarning(SOP_MESSAGE, "Node contains degenerate primitives");
        }
    }
    catch (const std::exception& e)
    {
        addError(SOP_MESSAGE, e.what());
    }

    return error();
}

OP_Node* SOP_GolaemImporter::create(OP_Network* net, const char* name, OP_Operator* op)
{
    return new SOP_GolaemImporter(net, name, op);
}
